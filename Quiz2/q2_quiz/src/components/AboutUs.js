
function AboutUs(props) {

    return(
        <div>
            <h2> จัดทำโดย : { props.name} </h2>
            <h3> ติดต่อ{props.name}ได้ { props.address} </h3>
            <h4> รหัสนักศึกษา {props.stdid} </h4>
        </div>
    );
}

export default AboutUs;