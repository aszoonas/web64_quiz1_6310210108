
import { Link } from "react-router-dom";
function Header() {


    return(

        <div align="left">
            ยินดีต้อนรับสู่เว็บพยากรณ์ : &nbsp;&nbsp;
            <Link to="/">พยากรณ์</Link>
            &nbsp;&nbsp;&nbsp;
            <Link to="/about">ผู้จัดทำ</Link>
            <hr />
        </div>

    );
}

export default Header;