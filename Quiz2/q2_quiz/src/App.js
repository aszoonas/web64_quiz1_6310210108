import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import { Routes, Route, Link } from "react-router-dom";
import AboutUsPages from './Pages/AboutUsPages';
import PredictPages from './Pages/PredictPages';


function App() {
  return (
    <div className="App">
      <Header />

      <Routes>
        
        <Route path="/" element={
                  <PredictPages />
                } />
        <Route path="/about" element={
                   <AboutUsPages />
                } />        
                  
      </Routes>
      
    </div>
  );
}

export default App;
