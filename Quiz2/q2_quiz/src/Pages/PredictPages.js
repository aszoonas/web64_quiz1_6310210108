
import { useState } from "react";
import Predict from "../components/Predict";


function PredictPages() {

    const [ name, setName ] = useState("");
    const [ years, setYears ] = useState("");
    const [ age, setAge ] = useState("");
    const [ answer, setAnswer ] = useState("");

    function Guess() {
        let y = parseFloat(years);
        let a = parseFloat(age);
        let luck = y / a
        setAnswer( luck );
        if (luck > 100) {
            setAnswer("พรุ่งนี้รวย")
        }else {
            setAnswer("พรุ่งนี้รวยแต่คนละข้อ")
        }
    }

    return (
        <div align="left">
            <div align="center">
                ยินดีต้อนรับสู่เว็บพยากรณ์
                <hr />

                คุณชื่อ : <input type="text"  value={name} 
                        onChange={ (e) => { setName(e.target.value); } } /> <br /><br />
                ปีเกิด : <input type="text" value={years} 
                        onChange={ (e) => { setYears(e.target.value); } }/> <br /><br />
                อายุ : <input type="text" value={age} 
                        onChange={ (e) => { setAge(e.target.value); } } /> <br /><br />

                <button onClick={ ()=>{ Guess() } }> ทำนาย </button>

                { answer != 0 &&
                    <div>
                        <hr />

                        ที่นี่คือผลการทำนาย

                        <Predict 
                            name = { name }
                            answer = { answer }
                        />
                    </div>
                }
                

            </div>

        </div>
    );
}

export default PredictPages;