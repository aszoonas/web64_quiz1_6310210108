
import AboutUs from "../components/AboutUs";

function AboutUsPages() {

    return(
        <div>
            <div align="center">
                <h2> คณะผู้จัดทำเว็บนี้ </h2>
                <hr />

                <AboutUs name ="ญาลินดา"
                  address ="ทุกที่"
                  stdid ="6310210108"/>
                  <hr />

            </div>
        </div>
    );
}

export default AboutUsPages;